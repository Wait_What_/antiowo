# v4
- [x] Update commands on site and git
- [x] New warning checking system
- [x] Set blacklist/whitelist limit to 10
- [x] Disable random punishments
- [x] Add null punishments
- [x] Disable edit detection

# v5 (huge update)
## To do (bot)
- [ ] Split executor into modules
- [ ] Merge tag and nick scanning
- [ ] Redesign stock messages
- [ ] AntiowoPass usage cap - 5/day
- [ ] Voting streak multiplier (1 + 0.1 ... 2)
 - [ ] DM user on every vote
- [ ] Token commands
  - [ ] Balance
  - [ ] Give (10%) tax
  - [ ] Shop (pass, premium, code)
- [ ] Voting leaderboard with ability to opt-out
- [ ] Mod enmap to only create an entry when it's non default
- [ ] `isPremium(gid)` function
- [ ] Custom messages
  - [ ] Command
  - [ ] Executor support
- [ ] Edit detection
- [ ] Channel whitelists
- [ ] Webhook logs
  - [ ] Command
  - [ ] Executor support
- [ ] Increase word lists to 30 for premium users
- [ ] Antiowo's message self-deletion
- [ ] New punishments
  - [ ] Mute
  - [ ] Random
- [ ] API for patreon oauth

# To do (website)
- [ ] Usage page
- [ ] Better commands page
- [ ] Redeem page
  - [ ] Patreon oauth
  - [ ] Graphics

## Antiowo Tokens
- Value
  - Earning
    - 1 vote = 10 tokens * multiplier
    - 1$ = 200, 3$ = 1000, 5$ = 2000, 10$ = 5000
      - User signs in on `antiowo.xyz/redeem` via patreon oauth
      - Table of gift codes is displayed (number, code, used by, value)
      - User redeems code using `::redeem`
  - Spending
    - 1 pass = 40 buy/20 sell
    - Antiowo Premium = 2000/month
- 10% tax for trading tokens

## Antiowo Premium features
- Custom messages
- Channel whitelists
- Webhook logs
- Extra whitelist/blacklist words (10 -> 30)
- Edit detection (toggle)
- Message self-deletion
- Random punishments and mutes
