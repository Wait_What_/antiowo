# Contributing
Feel free to make a PR or create an issue.

## Code style/rules
- No semicolons anywhere ever
- 4 spaces for indentation
- Don't add useless features
- Never use var
- Use a mix of ES6 and also not ES6

thank