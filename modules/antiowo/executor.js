const { bot, functions, db, modules, config } = require('../../bot')
const escape = require('discord.js').Util.escapeMarkdown

const timeouts = {
    complain: new Set(),
    warn: new Set(),
    delete: new Set(),
    spare: new Set()
}

exports.run = async (m, matches) => {
    db.ensure.guild(m.guild.id)
    const guild = db.guild.get(m.guild.id)

    const join = arr => {
        if (arr.length == 1) return `**${escape(arr[0])}**`
        if (arr.length == 2) return arr.map(w => `**${escape(w)}**`).join(' and ')
        if (arr.length > 2)
            return `${arr.slice(0, arr.length - 1).map(w => `**${escape(w)}**`).join(', ')} ` +
                `and **${escape(arr[arr.length - 1])}**`
    }

    const reasons = []
    matches.message && reasons.push(`Saying ${join(matches.message).slice(0, 32)}` +
        `${join(matches.message).length > 32 ? '...' : ''}`)
    matches.nickname && reasons.push(`Having ${join(matches.nickname).slice(0, 32)}` +
        `${join(matches.nickname).length > 32 ? '...' : ''} in the nickname`)
    matches.tag && reasons.push(`Having ${join(matches.tag).slice(0, 32)}` +
        `${join(matches.tag).length > 32 ? '...' : ''} in the tag`)

    const checkImmunity = () => {
        if (!guild.immunity) return false

        db.ensure.user(m.author.id)
        const count = db.user.get(m.author.id, 'passes')
        if (count < 1) return false

        db.user.dec(m.author.id, 'passes')

        if (timeouts.spare.has(m.author.id)) return true

        timeouts.spare.add(m.author.id)
        setTimeout(() => timeouts.spare.delete(m.author.id), 20 * 1000)

        const privateEmbed = functions.embed()
            .setAuthor('Antiowo spared you', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setDescription('Antiowo was about to destroy you but then Antiowo remembered that you have ' +
                'an **Antiowo Pass**! Antiowo forgives you for now.' +
                `\nYou now have **${count - 1}** pass${count == 2 ? '' : 'es'}`)
            .setFooter(`You used up 1 pass. Get more: ${functions.getPrefix(m.guild.id)}support`)

        const publicEmbed = functions.embed()
            .setAuthor(`Antiowo spared ${m.author.tag}`, bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setDescription(`This member had an Antiowo Pass, so Antiowo spared them. They now have **${count - 1}** passes.` +
                `\nUse \`${functions.getPrefix(m.guild.id)}support\` to get passes.`)
            .setFooter(`You can disable immunity using ${functions.getPrefix(m.guild.id)}immunity but please don't`)

        m.author.send({ embed: privateEmbed })
            .catch(() => {})

        m.channel.send({ embed: publicEmbed })
            .catch(() => {})
            .then(m2 => guild.selfDelete && m2.delete(15 * 1000).catch(() => {}))

        return true
    }

    const checkWarnings = () => {
        const warn = warnings => {
            const warningEmbed = functions.embed()
                .setAuthor('Stop right now, criminal!', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                .setThumbnail(m.author.displayAvatarURL)
                .setDescription(
                    `**${warnings || 1}**/**${guild.warningCount}** warnings\n` +
                    `**Reason${reasons.length > 1 ? 's' : ''}**:\n` +
                    (reasons.map(reason => `:white_small_square: ${reason}`).join('\n') || 'No reasons?')
                )
                .setFooter(`Help Antiowo grow and get Antiowo Passes! ${functions.getPrefix(m.guild.id)}support`)


            if (!timeouts.warn.has(m.author.id)) {
                timeouts.warn.add(m.author.id)
                setTimeout(() => timeouts.warn.delete(m.author.id), 10 * 1000)

                m.channel.send({ embed: warningEmbed })
                    .catch(() => {})
                    .then(m2 => guild.selfDelete && m2.delete(15 * 1000).catch(() => {}))
            }

            return true
        }


        if (guild.warningCount < 1) return false
        const warnings = guild.warnings[m.author.id]

        if (isNaN(warnings)) {
            db.guild.set(m.guild.id, 1, `warnings.${m.author.id}`)
            return warn(1)
        }

        if (warnings >= guild.warningCount) {
            db.guild.set(m.guild.id, 0, `warnings.${m.author.id}`)
            return false
        }

        db.guild.inc(m.guild.id, `warnings.${m.author.id}`)
        return warn(warnings + 1)
    }

    const sendEmbeds = (action, sendToMember) => {
        const publicEmbed = functions.embed()
            .setAuthor(`Antiowo ${action} ${m.author.tag}`, bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setThumbnail(m.author.displayAvatarURL)
            .setDescription(
                `Why did they get **${action}**?\n` +
                (reasons.map(reason => `:white_small_square: ${reason}`).join('\n') || 'No reasons found?')
            )
            .setFooter(`Help Antiowo grow and get Antiowo Passes! ${functions.getPrefix(m.guild.id)}support`)

        const privateEmbed = functions.embed()
            .setAuthor(`You were ${action} by Antiowo`, bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setThumbnail(m.author.displayAvatarURL)
            .setDescription(
                `Why did you get **${action}**?\n` +
                (reasons.map(reason => `:white_small_square: ${reason}`).join('\n') || 'No reasons found?')
            )
            .setFooter(`Help Antiowo grow and get Antiowo Passes! ${functions.getPrefix(m.guild.id)}support`)

        modules.get('firstRun').run(m)

        m.channel.send({ embed: publicEmbed })
            .catch(() => {})
            .then(m2 => guild.selfDelete && m2.delete(15 * 1000).catch(() => {}))

        if (!sendToMember) return null
        return m.author.send({ embed: privateEmbed })
            .catch(() => {})
    }

    const complain = (action, permission) => {
        if (timeouts.complain.has(m.author.id)) return
        else {
            timeouts.complain.add(m.author.id)
            setTimeout(() => timeouts.complain.delete(m.author.id), 25 * 1000)
        }

        const complainEmbed = functions.embed()
            .setAuthor('Stop right now, criminal!', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setDescription(`Antiowo **can't ${action}** because Antiowo either doesn't have **permission** ` +
                `to **\`${permission}\`** or this member's highest role is higher than Antiowo's. Move Antiowo's ` +
                `role higher up in the role list.\nIf this is intented, **whitelist** this member using ` +
                `\`${functions.getPrefix(m.guild.id)}userwhitelist @${m.author.tag}\``)
            .setFooter('This message will delete itself in 20 seconds')

        m.channel.send({ embed: complainEmbed })
            .then(m2 => m2.delete(20 * 1000).catch(() => {}))
            .catch(() => {})
    }

    const error = action => m.respond(':x: Make sure that Antiowo has **permission** to do this, and that ' +
        'Antiowo\'s role is **higher** than this member\'s **highest role**', `Whoops, the ${action} failed...`)

    const log = (action, error) => {
        if (!config.logAntiowo) return

        // This is just for testing and should be disabled in the config
        console.log(`${error ? 'FAILED: ' : ''}${action} ${m.author.tag} / ${m.author.id} for ${join(reasons)}`)

        // Advanced kill counter for the website at antiowo.xyz (WIP)
        if (modules.has('counter')) modules.get('counter').run(m, action, join(reasons), error)
    }

    let punishment = guild.punishment
    if (punishment == 4) punishment = Math.floor(Math.random() * 3)

    // Ban
    if (punishment == 0) {
        if (!(m.member.bannable && m.guild.members.get(bot.user.id).highestRole.position > m.member.highestRole.position))
            return complain('ban this member', 'ban members')

        if (checkImmunity() || checkWarnings()) return

        await sendEmbeds('banned', true)

        guild.delete && m.delete().catch(() => {})

        m.member.ban(`Automatically banned for: ${join(reasons.map(item => item.replace(/\*/g, '')))}`)
            .then(() => log('Banned', false))    
            .catch(() => {
                error('ban')
                log('Banned', true)
            })
    } else

    // Kick
    if (punishment == 1) {
        if (!(m.member.kickable && m.guild.members.get(bot.user.id).highestRole.position > m.member.highestRole.position))
            return complain('kick this member', 'kick members')

        if (checkImmunity() || checkWarnings()) return

        await sendEmbeds('kicked', true)

        guild.delete && m.delete().catch(() => {})
        
        m.member.kick(`Automatically kicked for: ${join(reasons.map(item => item.replace(/\*/g, '')))}`)
            .then(() => log('Kicked', false))  
            .catch(() => {
                error('kick')
                log('Kicked', true)
            })
    } else

    // Delete
    if (punishment == 2) {
        if (!(m.deletable && m.guild.members.get(bot.user.id).highestRole.position > m.member.highestRole.position))
            return complain('delete this message', 'manage messages')
        
        if (checkImmunity() || checkWarnings()) return

        const ignore = timeouts.delete.has(m.author.id)

        if (!ignore) {
            timeouts.delete.add(m.author.id)
            setTimeout(() => timeouts.delete.delete(m.author.id), 25 * 1000)

            sendEmbeds('censored', false)
        }

        m.delete()
            .then(() => !ignore && log('Deleted', false))
            .catch(() => {
                if (!ignore) {
                    error('message deletion')
                    log('Deleted', true)
                }
            })
    } else

    // None
    if (punishment == 3) {
        const ignore = timeouts.delete.has(m.author.id)

        if (!ignore) {
            timeouts.delete.add(m.author.id)
            setTimeout(() => timeouts.delete.delete(m.author.id), 10 * 1000)

            sendEmbeds('noticed', false)
        }
    }

    !db.bot.has('kills') && db.bot.set('kills', 0)
    db.bot.inc('kills')
}

exports.meta = {
    name: 'executor',
    autorun: 0
}