const getFileList = require('../../library/getFileList')

const commands = {
    names: new Map(),
    commands: new Map()
}
const help = new Map()

exports.help = help
exports.commands = commands

exports.run = () => {
    commands.names.clear()
    commands.commands.clear()
    help.clear()

    getFileList('./commands').forEach(commandDir => {
        commandDir = `../.${commandDir}`
        
        if (require.resolve(commandDir))
            delete require.cache[require.resolve(commandDir)]

        const command = require(commandDir)

        command.meta.names.forEach(name => commands.names.set(name, command.meta.names[0]))
        commands.commands.set(command.meta.names[0], command)

        if (command.meta.help.category) {
            if (command.meta.help.category.toLowerCase() == 'all')
                throw new Error('Command category can\'t be \'all\'')

            if (!help.has(command.meta.help.category)) help.set(command.meta.help.category, [])

            help.get(command.meta.help.category).push({
                names: command.meta.names,
                description: command.meta.help.description,
                usage: command.meta.help.usage,
                permissions: command.meta.permissions
            })
        }

        
    })
}

exports.meta = {
    name: 'commandLoader',
    autorun: 1
}