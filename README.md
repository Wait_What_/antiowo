# Antiowo

[![KILLS](https://flat.badgen.net/https/antiowo.xyz/api/badgen/kills)](https://antiowo.xyz)
[![SERVERS](https://flat.badgen.net/https/antiowo.xyz/api/badgen/servers)](https://antiowo.xyz)
[![VOTES](https://flat.badgen.net/https/antiowo.xyz/api/badgen/votes?color=e0aa11)](https://vote.antiowo.xyz)

[![INVITE](https://flat.badgen.net/badge/Invite/Antiowo/e47412)](https://inv.antiowo.xyz)
[![COMMANDS](https://flat.badgen.net/badge/Commands/View/43b581)](https://gitlab.com/BarkingDog/antiowo/blob/master/commands.md)
[![SUPPORTSERVER](https://flat.badgen.net/badge/Support%20server/Join/purple)](https://discord.gg/N8Fqcuk)
[![WEBSITE](https://flat.badgen.net/badge/Website/antiowo.xyz/cyan)](https://antiowo.xyz)

[![MIT](https://flat.badgen.net/badge/License/MIT/blue)](https://gitlab.com/Wait_What_/antiowo/blob/master/LICENSE.md)
[![NODE](https://flat.badgen.net/badge/Language/Node.js/green?icon=node)](https://nodejs.org/en/)
[![DISCORDBOTBOILERPLATE](https://flat.badgen.net/badge/Made%20using/discord-bot-boilerplate/blue)](https://gitlab.com/Wait_What_/discord-bot-boilerplate)

Are you tired of weebs and furries in your Discord server? Antiowo will ban, kick or otherwise censor anyone 
who says `owo` or `uwu`! You can also blacklist and whitelist your own words, users and roles.

# Features
- Removal of messages that include `owo` or `uwu`. Antiowo is very smart and will detect much more than just that
- Warning system. You can set the amount of warnings before action
- Custom punishments. You can switch between bans, kicks or simple deletions (censors)
- Rewards for voting. [Voting for Antiowo](https://vote.antiowo.xyz) 3 times gives you 1 Antiowo Pass
    - This can be disabled in your server

# Notes
- The default prefix is `::`. It can be changed using `::prefix`
- Use `::help all` for more info
    - The commands can be found [HERE](https://gitlab.com/Wait_What_/antiowo/commands.md)

# Antiowo doesn't work?
- If Antiowo doesn't work check out the `Having issues?` section on [Antiowo's website](https://antiowo.xyz).
	Make sure that the bot has a higher role than the offender and that Antiowo's role can manage messages and
    ban members. You can also [join the support server](https://discord.gg/N8Fqcuk) for more help

[![Discord Bots](https://discordbots.org/api/widget/upvotes/495496088510529556.svg)](https://discordbots.org/bot/495496088510529556)
Click here to vote for Antiowo

# Installation
Install `git` and `nodejs`
```
git clone https://gitlab.com/Wait_What_/antiowo.git
cd antiowo
npm install # You may have issues with `better-sqlite3`
mv config_template.js config.js # and configure
node bot
```

> *Note: this bot is meant to be joke. Please do not take it personally.*