module.exports = {
    tokens: {
        discord: '',
    },
    defaultPrefix: '::',
    owners: [
        ''
    ],
    blacklists: {
        disabled: null,
        basic: /\b(owo|uwu)\b/gi,
        regular: /\b(owo|uwu)\b/gi, // Change them if you wish
        strict: /\b(owo|uwu)\b/gi
    },
    shards: 'auto',
    name: 'Antiowo-clone',
    embedColor: '#ffffff',
    activity: {
        text: 'BOTservers servers | BOTprefixhelp',
        type: 'PLAYING'

    },
    logCommands: { // Debug
        enabled: false,
        ignoreBotOwners: false
    },
    logAntiowo: false, // Debug
    dbDefaults: {
        user: {
            blacklist: false,
            whitelist: false,

            passes: 0
        },
        guild: {
            prefix: undefined,

            punishment: 1, // 0 - ban, 1 - kick, 2 - deletion, 3 - none
            strictness: 2, // 0 - disabled, 1 - basic (owo), 2 - regular (0ww.u), 3 - strict (coworker)
            tildeScanner: false,
            nicknameScanner: true,
            delete: true,

            warnings: {},
            warningCount: 0,

            wordBlacklist: [],
            wordWhitelist: [],
            userWhitelist: [],
            roleWhitelist: [],

            immunity: true,
            firstRun: true,
            ignore: false
        }
    }
}
