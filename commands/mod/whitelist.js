const { functions, bot, db } = require('../../bot')
const escape = require('discord.js').Util.escapeMarkdown

exports.run = async (m, a) => {
    const displaySyntax = err => m.respond(
        (err ? `:x: **Error**: ${err}\n\n` : '') +
        `**Usage**: \`${functions.getPrefix(m.guild.id)}whitelist <action> <list> [name]\`\n` +
        ':white_small_square: Actions: `add`, `remove`, `show`, `clear`\n' +
        ':white_small_square: Lists: `word`, `role`, `user`\n' +
        '**Examples**:\n' +
        `:small_blue_diamond: Whitelist the word \`owo\` - \`${functions.getPrefix(m.guild.id)}whitelist add word owo\`\n` +
        `:small_blue_diamond: Unwhitelist a user named \`user#1234\` - \`${functions.getPrefix(m.guild.id)}` +
            'whitelist remove user @user#1234\`\n' +
        `:small_blue_diamond: See all whitelisted roles - \`${functions.getPrefix(m.guild.id)}whitelist show role\`\n`,
    'Whitelists')

    if (a.length < 2) return displaySyntax()

    const [ name, member, embed ] = [
        a.slice(2).join(' ').toLowerCase(),
        m.mentions.members.first() || m.guild.members.get(a[3]),
        functions.embed()
    ]

    let [ action, listName, i ] = [ a[0].toLowerCase(), a[1].toLowerCase(), 0 ]

    // Flip action and listName if required
    if (listName.match(/^(a|r|s|l|clear)/i) && action.match(/^(w|r|u)/i))
        [ action, listName ] = [ listName, action ]

    // Word list
    if (listName.startsWith('w')) {
        const whitelist = db.guild.get(m.guild.id, 'wordWhitelist')

        // Add word
        if (action.startsWith('a')) {
            if (!name) return displaySyntax('you didn\'t provide a word')

            if (whitelist.length > 9)
                return m.respond(':x: The limit of 10 words was reached. Remove some words first')
            if (name.length < 2 || name.length > 20) return m.respond(':x: The word must be 2-20 characters long')
            if (whitelist.includes(name)) return m.respond(':x: This word is already whitelisted')

            db.guild.push(m.guild.id, name, 'wordWhitelist')
            m.respond(`:white_check_mark: Whitelisted the word **${escape(name)}**`)

            functions.updateRegex(m.guild.id)
            return
        } else

        // Remove word
        if (action.startsWith('r')) {
            if (!name) return displaySyntax('you didn\'t provide a word')

            if (name.length < 2) return m.respond(':x: The word must be at least 2 characters long')
            if (whitelist.length < 1) return m.respond(':x: There are no words to remove from the whitelist')
            if (!whitelist.includes(name))
                return m.respond(':x: This word is not whitelisted. Verify that it\'s exactly the same')

            db.guild.remove(m.guild.id, name, 'wordWhitelist')
            m.respond(`:white_check_mark: Removed the word **${escape(name)}** from the whitelist`)

            functions.updateRegex(m.guild.id)
            return
        } else

        // Show words
        if (action.startsWith('s') || action.startsWith('l')) {
            embed
                .setAuthor('Word whitelist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                .setDescription(
                    whitelist
                        .map(word => `${++i}. **${escape(word)}**`)
                        .join('\n') || '_Empty_'
                )
                .setFooter(`${i} / 10 words`)
        } else

        // Clear words
        if (action == 'clear') {
            db.guild.set(m.guild.id, [], 'wordWhitelist')
            functions.updateRegex(m.guild.id)

            m.respond(':white_check_mark: Cleared the word whitelist')
            return
        }

        else return displaySyntax('You did not provide a valid action or list name')
    } else

    // Role list
    if (listName.startsWith('r')) {
        const whitelist = db.guild.get(m.guild.id, 'roleWhitelist')

        // Add role
        if (action.startsWith('a')) {
            if (!name) return displaySyntax('you didn\'t provide a role name')

            const roles = m.guild.roles.filter(role => role.name.toLowerCase() == name || role.id == name)
            if (roles.size < 1) return m.respond(':x: This role wasn\'t found. Verify that the name or ID is exactly the same')
            if (roles.size > 1) {
                embed
                    .setAuthor('Role whitelist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                    .setDescription(
                        ':x: There is **more than one role** with the **same name**. ' +
                        `Use the **role ID** instead of the name. Example: \`${functions.getPrefix(m.guild.id)}` +
                        `whitelist add role ${roles.first().id}\`\n\n` +
                        'Roles sorted by **position**:\n' +
                        roles
                            .sort((a, b) => b.position - a.position)
                            .map(role => `:white_small_square: ${m.guild.roles.size - role.calculatedPosition}. ` +
                                `**${escape(role.name)}** (${m.guild.members.filter(user => user.roles.has(role.id)).size}` +
                                ` users) - \`${role.id}\``)
                            .join('\n')
                    )
            } else {
                const role = roles.first()

                if (whitelist.includes(role.id)) return m.respond(':x: This role is already whitelisted')

                db.guild.push(m.guild.id, role.id, 'roleWhitelist')
                m.respond(`:white_check_mark: Whitelisted the role **${role.name}**`)
                return
            }
        } else

        // Remove role
        if (action.startsWith('r')) {
            const roles = whitelist
                .filter(role => m.guild.roles.has(role))
                .map(role => m.guild.roles.get(role))
                .filter(role => role.name.toLowerCase() == name || role.id == name)

            if (roles.length < 1) return m.respond(':x: This role is not whitelisted')
            if (roles.length > 1) {
                embed
                    .setAuthor('Role whitelist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                    .setDescription(
                        ':x: There is **more than one role** with the **same name**. ' +
                        `Use the **role ID** instead of the name. Example: \`${functions.getPrefix(m.guild.id)}` +
                        `whitelist remove role ${roles[0].id}\`\n\n` +
                        'Roles sorted by **position**:\n' +
                        roles
                            .sort((a, b) => b.position - a.position)
                            .map(role => `:white_small_square: ${m.guild.roles.size - role.calculatedPosition}. ` +
                                `**${escape(role.name)}** (${m.guild.members.filter(user => user.roles.has(role.id)).size}` +
                                ` users) - \`${role.id}\``)
                            .join('\n')
                    )
            } else {
                const role = roles[0]

                db.guild.set(m.guild.id, whitelist.filter(r => m.guild.roles.has(r) && r != role.id), 'roleWhitelist')
                m.respond(`:white_check_mark: Removed the role **${role.name}** from the whitelist`)
                return
            }
        } else

        // Show roles
        if (action.startsWith('s') || action.startsWith('l')) {
            embed
                .setAuthor('Role whitelist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                .setDescription(
                    whitelist
                        .filter(rid => m.guild.roles.has(rid))
                        .map(rid => `${++i}. **${escape(m.guild.roles.get(rid).name)}**`)
                        .join('\n') || '_Empty_'
                )
        } else

        // Clear roles
        if (action == 'clear') {
            db.guild.set(m.guild.id, [], 'roleWhitelist')
            m.respond(':white_check_mark: Cleared the role whitelist')
            return
        }

        else return displaySyntax('You did not provide a valid action or list name')
    } else

    // User list
    if (listName.startsWith('u')) {
        const whitelist = db.guild.get(m.guild.id, 'userWhitelist')

        // Add user
        if (action.startsWith('a')) {
            if (!member) return displaySyntax('you didn\'t mention a valid member')
            if (whitelist.includes(member.id)) return m.respond(':x: This memeber is already whitelisted')

            db.guild.push(m.guild.id, member.id, 'userWhitelist')
            m.respond(`:white_check_mark: Whitelisted the user **${member.user.tag}**`)
            return
        } else

        // Remove user
        if (action.startsWith('r')) {
            if (!member) return displaySyntax('you didn\'t mention a valid member')
            if (!whitelist.includes(member.id)) return m.respond(':x :This member is not whitelisted')

            db.guild.remove(m.guild.id, member.id, 'userWhitelist')
            m.respond(`:white_check_mark: Removed the user **${member.user.tag}** from the whitelist`)
            return
        } else

        // List users
        if (action.startsWith('s') || action.startsWith('l')) {
            embed
                .setAuthor('User whitelist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
                .setDescription(
                    whitelist
                        .filter(uid => m.guild.members.has(uid))
                        .map(uid => `${++i}. <@${uid}>`)
                        .join('\n') || '_Empty_'
                )
        } else

        // Clear users
        if (action == 'clear') {
            db.guild.set(m.guild.id, [], 'userWhitelist')
            m.respond(':white_check_mark: Cleared the user whitelist')
            return
        }

        else return displaySyntax('You did not provide a valid action or list name')
    }

    else return displaySyntax('You did not provide a valid action or list name')

    m.channel.send({ embed })
}

exports.meta = {
    names: ['whitelist', 'wl', 'wordwhitelist', 'rolewhitelist', 'userwhitelist'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Manage all whitelists',
        usage: 'action list [input]',
        category: 'mod'
    }
}