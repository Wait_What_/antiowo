const { functions, bot, db } = require('../../bot')
const escape = require('discord.js').Util.escapeMarkdown

exports.run = async (m, a) => {
    const displaySyntax = () => m.respond(
        `**Usage**: \`${functions.getPrefix(m.guild.id)}blacklist <action> [name]\`\n` +
        ':white_small_square: Actions: `add`, `remove`, `show`, `clear`\n' +
        '**Examples**:\n' +
        `:small_blue_diamond: Blacklist \`>_<\` - \`${functions.getPrefix(m.guild.id)}blacklist add >_<\`\n` +
        `:small_blue_diamond: Unblacklist \`wow\` - \`${functions.getPrefix(m.guild.id)}blacklist remove wow\`\n` +
        `:small_blue_diamond: See all blacklisted words - \`${functions.getPrefix(m.guild.id)}blacklist show\`\n`,
    'Word blacklist')

    if (a.length < 1) return displaySyntax()

    const [ action, name, blacklist, embed ] = [
        a[0].toLowerCase(),
        a.slice(1).join(' ').toLowerCase(),
        db.guild.get(m.guild.id, 'wordBlacklist'),
        functions.embed()
    ]

    let i = 0

    // Add word
    if (action.startsWith('a')) {
        if (!name) return displaySyntax()

        if (blacklist.length > 9)
            return m.respond('The limit of 10 words was reached. Remove some words first')
        if (name.length < 2 || name.length > 20) return m.respond('The word must be 2-20 characters long')
        if (blacklist.includes(name)) return m.respond('This word is already blacklisted')

        db.guild.push(m.guild.id, name, 'wordBlacklist')
        m.respond(`Blacklisted the word **${escape(name)}**`)

        functions.updateRegex(m.guild.id)
        return
    } else

    // Remove word
    if (action.startsWith('r')) {
        if (!name) return displaySyntax()

        if (name.length < 2) return m.respond('The word must be at least 2 characters long')
        if (blacklist.length < 1) return m.respond('There are no words to remove from the blacklist')
        if (!blacklist.includes(name))
            return m.respond('This word is not blacklisted. Verify that it\'s exactly the same')

        db.guild.remove(m.guild.id, name, 'wordBlacklist')
        m.respond(`Removed the word **${escape(name)}** from the blacklist`)

        functions.updateRegex(m.guild.id)
        return
    } else

    // Show words
    if (action.startsWith('s') || action.startsWith('l')) {
        embed
            .setAuthor('Word blacklist', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
            .setDescription(
                blacklist
                    .map(word => `${++i}. **${escape(word)}**`)
                    .join('\n') || '_Empty_'
            )
            .setFooter(`${i} / 10 words`)
    } else

    // Clear words
    if (action == 'clear') {
        db.guild.set(m.guild.id, [], 'wordBlacklist')
        functions.updateRegex(m.guild.id)

        m.respond('Cleared the word blacklist')
        return
    }

    else return displaySyntax()

    m.channel.send({ embed })
}

exports.meta = {
    names: ['blacklist', 'bl', 'wordblacklist'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Manage the word blacklist',
        usage: 'action [input]',
        category: 'mod'
    }
}